export default {
    setSources (state, sources) {
        state.sources = sources
    },
    setNews (state, news) {
        state.news = news
    },
    addNews (state, news) {
        state.news = [...state.news, ...news]
    },
    addBookmark (state, bookmark) {
        state.bookmarks = [...state.bookmarks, { ...bookmark }]
        localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks))
    },
    removeBookmark (state, payload) {
        state.bookmarks = state.bookmarks.filter(bookmark => bookmark.title !== payload.title)
        localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks))
    },
    setLoading (state, isLoading) {
        state.isLoading = isLoading
    },
    setTheme (state) {
        state.darkMode = !state.darkMode
        localStorage.setItem('darkMode', state.darkMode)
    },
    setSearchParams (state, params) {
        state.searchParams = params
    },
    setNewsPage (state) {
        state.newsPage = 1
    },
    setNextPage (state) {
        state.newsPage += 1
    },
    setCanFetchNews (state, canFetch) {
        state.canFetchNews = canFetch
    }
}