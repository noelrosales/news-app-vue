import getNews from '@/services/getNews'
import getSources from '@/services/getSources'

export default {
    async loadNews ({ commit, state }) {
        commit('setLoading', true)
        commit('setNewsPage')
        commit('setNews', [])
        try {
            const { articles, totalResults } = await getNews(state.searchParams)
            commit('setNews', articles)
            if(state.news.length < totalResults) {
            commit('setNextPage')
            commit('setCanFetchNews', true)
            } else {
            commit('setCanFetchNews', false)
            }
        } catch (error) {
            commit('setNews', [])
        }
        commit('setLoading', false)
    },
    async loadMoreNews ({ commit, state }) {
        const nextParams = {...state.searchParams, page: state.newsPage}
        commit('setLoading', true)
        try {
            const { articles, totalResults } = await getNews(nextParams)
            commit('addNews', articles)
            if(state.news.length < totalResults) {
            commit('setNextPage')
            commit('setCanFetchNews', true)
            } else {
            commit('setCanFetchNews', false)
            }
        } catch (error) {
            commit('setNews', [])
        }
        commit('setLoading', false)
    },
    async loadSources ({ commit }) {
        commit('setLoading', true)
        try {
            const sources = await getSources()
            commit('setSources', sources)
        } catch (error) {
            commit('setSources', [])
        }
        commit('setLoading', false)
    },
    addBookmark ({ commit }, bookmark) {
        commit('addBookmark', bookmark)
    },
    removeBookmark ({ commit }, bookmark) {
        commit('removeBookmark', bookmark)
    },
    toggleTheme ({ commit }) {
        commit('setTheme')
    }
}