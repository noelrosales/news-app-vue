export default {
    getNews: state => {
        return state.news
    },
    getSources: state => {
        return state.sources
    },
    getBookmarks: state => {
        return state.bookmarks
    },
    getDarkMode:  state => {
        return state.darkMode
    }
}