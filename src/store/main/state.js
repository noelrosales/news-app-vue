export default {
    darkMode: localStorage.getItem('darkMode') || true,
    sources: [],
    news: [],
    newsPage: 1,
    canFetchNews: false,
    searchParams: {},
    bookmarks: JSON.parse(localStorage.getItem('bookmarks')) || [],
    isLoading: false
}